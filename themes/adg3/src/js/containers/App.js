import React from 'react';
import { Provider } from 'react-redux'
import thunkMiddleware from 'redux-thunk'
import createLogger from 'redux-logger'
import { createStore, applyMiddleware } from 'redux'

import rootReducer from '../reducers'
import AsyncUser from './AsyncUser'
import LatestFromBlog from './LatestFromBlog'

const loggerMiddleware = createLogger()

export function configureStore(initialState) {
  return createStore(
    rootReducer,
    applyMiddleware(
      thunkMiddleware,
      loggerMiddleware
    )
  )
}

const store = configureStore()

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <LatestFromBlog />
      </Provider>
    );
  }
};

export default App
