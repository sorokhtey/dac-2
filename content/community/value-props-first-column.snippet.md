<h2>Contribute to  the docs</h2>

Build better together. Be part of a thriving community of developers building on the Atlassian platform. <br><br> <a href="/cloud/jira/platform/getting-started/">Check out the docs <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
