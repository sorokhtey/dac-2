---
title: Theming 44064007
aliases:
    - /confcloud/theming-44064007.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=44064007
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=44064007
confluence_id: 44064007
platform:
product:
category:
subcategory:
---
# Confluence Connect : Theming

## What is theming?

Confluence Connect themes change the look and feel of Confluence Cloud. Your theme can change styling for the header, spaces, pages, and blog posts.

Themes aren't just for show, though. They can help emphasize parts of the UI and influence user behavior in Confluence.

## How do I build a Confluence theme?

Check out our tutorial on [Theming with Confluence Connect]. It'll walk you through adding a new Confluence theme to your Connect descriptor, replacing the space home page, styling page elements, and more.

### Confluence header

Style the Confluence header, including buttons, menu items and the search field.

### Space home

Define a new space home page experience for spaces with your theme selected.

### Pages and blog posts

Change the color of headings, text, menu items, backgrounds, and more. Set the appearance of hovered items, add background imagery--it's up to you.

## What other patterns might be helpful?

### Page extensions

Add extra information or introduce new actions to the page byline.

## Let's do this!

Get started with our [theming tutorial][Theming with Confluence Connect].

  [Theming with Confluence Connect]: /confcloud/theming-with-confluence-connect-44063934.html

