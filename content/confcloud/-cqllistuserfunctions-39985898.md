---
title: Cqllistuserfunctions 39985898
aliases:
    - /confcloud/-cqllistuserfunctions-39985898.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985898
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985898
confluence_id: 39985898
platform:
product:
category:
subcategory:
---
# Confluence Connect : \_CQLListUserFunctions

-   [currentUser()]

  [currentUser()]: https://developer.atlassian.com/display/CONFDEV/CQL+Function+Reference#CQLFunctionReference-currentUser()

