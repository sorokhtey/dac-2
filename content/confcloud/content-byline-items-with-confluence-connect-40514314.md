---
title: Content Byline Items with Confluence Connect 40514314
aliases:
    - /confcloud/content-byline-items-with-confluence-connect-40514314.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=40514314
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=40514314
confluence_id: 40514314
platform:
product:
category:
subcategory:
---
# Confluence Connect : Content Byline Items with Confluence Connect

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td>Description</td>
<td>A step-by-step introduction to building a Content Byline item in Confluence</td>
</tr>
<tr class="even">
<td>Level</td>
<td><div class="content-wrapper">
2 - BEGINNER
</div></td>
</tr>
<tr class="odd">
<td>Estimated Time</td>
<td>25 minutes</td>
</tr>
<tr class="even">
<td>Example</td>
<td>N/A</td>
</tr>
</tbody>
</table>

 

These lessons will get you started writing your very first Confluence Connect byline item step-by-step.

Want to dive in quickly and build a macro in 10 minutes? Check out the [Quick Start Guide to Confluence Connect].

# Prerequisites

Ensure you have installed all the tools you need for Confluence Connect add-on development, and running Confluence by going through the [Development Setup].

# Lessons

In this series, we build a basic 'Page Approval' byline item. 

 

<table>
<colgroup>
<col width="25%" />
<col width="25%" />
<col width="25%" />
<col width="25%" />
</colgroup>
<thead>
<tr class="header">
<th><div class="tablesorter-header-inner">
Title
</div></th>
<th><div class="tablesorter-header-inner">
Description
</div></th>
<th><div class="tablesorter-header-inner">
Estimated Time
</div></th>
<th><div class="tablesorter-header-inner">
Level
</div></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><a href="https://developer.atlassian.com/display/CONFCLOUD/Lesson+1+-+Introduction+to+the+Byline">Lesson 1 - Introduction to the Byline</a></td>
<td>Add a simple 'approval' byline item under all pages in Confluence.</td>
<td>15 minutes</td>
<td><div class="content-wrapper">
<p>2 - BEGINNER</p>
</div></td>
</tr>
<tr class="even">
<td><a href="https://developer.atlassian.com/display/CONFCLOUD/Lesson+2+-+Dynamically+Update+your+Byline">Lesson 2 - Dynamically Update your Byline</a></td>
<td>Link your byline item with a content property stored in Confluence to allow for dynamic UI updates.</td>
<td>10 minutes</td>
<td><div class="content-wrapper">
<p>2 - BEGINNER</p>
</div></td>
</tr>
</tbody>
</table>

# Further reading

-   <a href="http://connect.atlassian.com/" class="external-link">Atlassian Connect Documentation</a>

  [Quick Start Guide to Confluence Connect]: https://developer.atlassian.com/display/CONFCLOUD/Quick+start+to+Confluence+Connect
  [Development Setup]: https://developer.atlassian.com/confcloud/development-setup-39988911.html
  [Lesson 1 - Introduction to the Byline]: https://developer.atlassian.com/display/CONFCLOUD/Lesson+1+-+Introduction+to+the+Byline
  [Lesson 2 - Dynamically Update your Byline]: https://developer.atlassian.com/display/CONFCLOUD/Lesson+2+-+Dynamically+Update+your+Byline

