---
title: Confluence Json Rpc APIs 39985836
aliases:
    - /confcloud/confluence-json-rpc-apis-39985836.html
dac_edit_link: https://developer.atlassian.com/pages/editpage.action?cjm=wozere&pageId=39985836
dac_view_link: https://developer.atlassian.com/pages/viewpage.action?cjm=wozere&pageId=39985836
confluence_id: 39985836
platform:
product:
category:
subcategory:
---
# Confluence Connect : Confluence JSON-RPC APIs

The JSON-RPC API is **deprecated since Confluence 5.5**. Confluence has a new [REST API] that is progressively replacing our existing APIs. We recommend plugin developers use the new REST APIs where possible.

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>Available:</p></td>
<td><p>The API ships with Confluence 4.1 and later. The plugin is compatible with Confluence 3.5 and later.</p>
<p>It was deprecated in Confluence 5.5.</p></td>
</tr>
<tr class="even">
<td><p>Skip to:</p></td>
<td><p>Go directly to the <a href="https://developer.atlassian.com/display/CONFCLOUD/Confluence+JSON-RPC+APIs#ConfluenceJSON-RPCAPIs-MethodsAvailable">Methods Available</a>.</p></td>
</tr>
</tbody>
</table>

The <a href="https://marketplace.atlassian.com/plugins/com.atlassian.confluence.rpc.confluence-json-rpc-plugin" class="external-link">Confluence JSON-RPC API (also available as a plugin</a>) exposes all the existing Confluence SOAP services via the <a href="http://groups.google.com/group/json-rpc/web/json-rpc-2-0" class="external-link">JSON-RPC 2.0</a> standard. It also supports a 'light' mode where you can supply the method name in the URL, removing the need to have an RPC-specific envelope around your JSON request data.

**On this page:**

-   [Quick Start]
-   [Why JSON-RPC]
-   [How the API is Implemented]
-   [Getting Started]
-   [The Light Protocol]
-   [Authentication]
-   [Date Conversion]
-   [Methods Available ]
-   [Methods Exposed by Plugins]
-   [Named Parameters]
-   [Browsing and Testing your Remote APIs from the Confluence UI]

## Quick Start

1.  The JSON-RPC API is bundled with Confluence 4.1 and later. If you are using Confluence 3.5 to 4.0, <a href="https://marketplace.atlassian.com/plugins/com.atlassian.confluence.rpc.confluence-json-rpc-plugin" class="external-link">install the plugin</a> from Atlassian Marketplace.
2.  Read the documentation about [Confluence XML-RPC and SOAP APIs].
3.  Decide on your authentication mechanism. Token authentication is not supported. Your RPC client must log in via basic auth, OAuth or piggybacking on an existing login session.
4.  Send `POST` requests to the Confluence API exposed via JSON-RPC at `/rpc/json-rpc/confluenceservice-v2`. *Note:* Leave out the "token" argument of each method in the spec.

-   You may also wish to read the <a href="http://www.jsonrpc.org/specification" class="external-link">JSON-RPC 2.0 Specification</a>.

## Why JSON-RPC

SOAP messaging is unfriendly to many developers, especially those wanting to make calls to Confluence in a browser AJAX environment. We have therefore added a <a href="http://json-rpc.org/" class="external-link">JSON-RPC</a> API capability to Confluence, as a more JSON/JavaScript friendly mechanism.

## How the API is Implemented

The JSON-RPC API is provided by an open source plugin that is bundled with Confluence 4.1 and later. You can find the plugin source <a href="https://bitbucket.org/cmiller_atlassian/confluence-json-rpc-plugin/overview" class="external-link">on BitBucket</a>.

The plugin uses the Java server code that makes up the existing SOAP interface in Confluence, and converts the methods to JSON-RPC methods. There is a one-to-one mapping between the methods and parameters in the Confluence [SOAP API][Confluence XML-RPC and SOAP APIs] and the equivalent JSON-RPC methods and parameters. The plugin uses Jackson as the JSON library to provide the mapping between a Java method and JSON-RPC invocation of that method.

The main difference between the SOAP APIs and the JSON-RPC APIs is the <a href="http://json-rpc.org/wiki/specification" class="external-link">JSON-RPC wire format</a>.

-   Make your **SOAP**calls as a POST on the following path:

        http://your-confluence.example.com/confluence/rpc/soap-axis/confluenceservice-v2

-   Make your **JSON-RPC**calls as a POST on this path:

        http://your-confluence.example.com/confluence/rpc/json-rpc/confluenceservice-v2

Responses are delivered as MIME type `application/json`.

The plugin supports notifications and batch requests as per the JSON-RPC standard, and will make a best effort at serving JSON-RPC 1.0 requests.

The [JIRA JSON-RPC APIs] use the same code and work in a similar manner to Confluence.

## Getting Started

You must use the HTTP **POST** verb. You cannot use GET to invoke JSON-RPC methods.

**Request:** HTTP POST to `/rpc/json-rpc/confluenceservice-v2`

Sample input data:

``` syntaxhighlighter-pre
{"jsonrpc" : "2.0",  "method" : "getSpace",  "params" : [ "DOC" ],  "id" : 12345}
```

**Response:**

``` syntaxhighlighter-pre
{"jsonrpc" : "2.0", "result" : {
     "key" : "DOC", "name" : "Documentation Space",
     "url" : "http://wiki.example.com/display/DOC",
     "homePage" : "Home", "description" : "Product Documentation" },
  "id" : 12345 }
```

## The Light Protocol

The plugin also supports a 'light' protocol that allows clients to encode the method name in the URL, and provide only the method arguments in the request body. Non-error responses in the light protocol also leave out the RPC envelope and provide only what would normally be the 'result' object in a regular JSON-RPC response.

In summary:

-   Method is determined by the URL.
-   Request body is a JSON array of arguments. (Named parameters are dealt with in the same way as the heavy protocol.)
-   Response is the raw method result with no result envelope, or a full JSON-RPC error struct if an error occurs.

**Request:** HTTP POST to `/rpc/json-rpc/confluenceservice-v2/getSpace`

Sample input data:

``` syntaxhighlighter-pre
[ "DOC" ]
```

**Response:**

``` syntaxhighlighter-pre
{ "key" : "DOC", "name" : "Documentation Space",
  "url" : "http://wiki.example.com/display/DOC",
  "homePage" : "Home", "description" : "Product Documentation" }
```

It is your choice whether to use the full or the light JSON-RPC format. One advantage of the light format is that access logs will contain the method invoked, because it is in the URL. The heavy format will not, because the method is inside the POST data.

## Authentication

In Confluence, the plugin does not support the token-based authentication that is normally used by Confluence SOAP plugins. JSON-RPC clients must acquire a valid login either by running in the browser of someone who is already logged in (session authentication), or must log in via basic auth or OAuth.

If a SOAP plugin is configured to support token authentication (that is, it declares `<authenticate>true</authenticate>` in its module descriptor):

-   The `login()` method will not be available via JSON-RPC.
-   You must leave out the token argument for each method when calling the JSON-RPC equivalent. For example:
    -   SOAP method: `storePage(token, page)`
    -   JSON-RPC method: `storePage(page)`

## Date Conversion

Because JSON has no native date type, dates are represented in JSON as the number of milliseconds since the epoch, GMT. In JavaScript you can pass this number into the Date() constructor to get a Date object, and you can get it back out by calling Date.getTime().

## Methods Available 

-   [Confluence JSON-RPC API Methods]
-   [Confluence JSON-RPC API Data Objects]

Additionally, the methods available to the JSON-RPC API are all defined in the [Confluence SOAP API Javadoc]. You must translate from the Java structure into JSON objects. Use the normal JavaBean rules for translating from one into the other.

## Methods Exposed by Plugins

Each plugin is available at `/rpc/json-rpc/XXXXX`, where `XXXXX` is the service path of the plugin.

For example, a SOAP service with a WSDL file at ` http://confluence.example.com/confluence/rpc/soap-axis/myservice?wsdl` would have a JSON-RPC URL of ` http://confluence.example.com/confluence/rpc/json-rpc/myservice`.

Light requests are made by appending `/methodName` to the regular resource URL.

## Named Parameters

JSON-RPC supports two forms of parameter encoding:

-   By position. Parameters are passed as an array of values. These are converted into Java method calls by the plugin as you would expect.
-   By name. Parameters are passed as an object containing named key/value pairs. Because Java does not support named parameters, any such call is converted into a by-position call that takes a single object argument.

In other words, the two following requests are equivalent:

``` syntaxhighlighter-pre
{"jsonrpc" : "2.0",  "method" : "storePage",  "params" : 
   {"space" : "DOC", "title" : "JSON-RPC Documentation" }}
```

``` syntaxhighlighter-pre
{"jsonrpc" : "2.0",  "method" : "storePage",  "params" :
   [ {"space" : "DOC", "title" : "JSON-RPC Documentation" } ] }
```

## Browsing and Testing your Remote APIs from the Confluence UI

You can browse and test your remote API using the REST Application Browser (RAB). The RAB is a page in the application administrative console. For information on using the RAB, see the  [documentation for the REST API Browser] .

##### RELATED TOPICS

[Confluence XML-RPC and SOAP APIs]
[JIRA JSON-RPC Overview][JIRA JSON-RPC APIs]

  [REST API]: /confcloud/confluence-rest-api-39985291.html
  [Methods Available]: https://developer.atlassian.com/display/CONFCLOUD/Confluence+JSON-RPC+APIs#ConfluenceJSON-RPCAPIs-MethodsAvailable
  [Quick Start]: #ConfluenceJSON-RPCAPIs-QuickStart
  [Why JSON-RPC]: #ConfluenceJSON-RPCAPIs-WhyJSON-RPC
  [How the API is Implemented]: #ConfluenceJSON-RPCAPIs-HowtheAPIisImplemented
  [Getting Started]: #ConfluenceJSON-RPCAPIs-GettingStarted
  [The Light Protocol]: #ConfluenceJSON-RPCAPIs-TheLightProtocol
  [Authentication]: #ConfluenceJSON-RPCAPIs-Authentication
  [Date Conversion]: #ConfluenceJSON-RPCAPIs-DateConversion
  [Methods Available ]: #ConfluenceJSON-RPCAPIs-MethodsAvailable
  [Methods Exposed by Plugins]: #ConfluenceJSON-RPCAPIs-MethodsExposedbyPlugins
  [Named Parameters]: #ConfluenceJSON-RPCAPIs-NamedParameters
  [Browsing and Testing your Remote APIs from the Confluence UI]: #ConfluenceJSON-RPCAPIs-BrowsingandTestingyourRemoteAPIsfromtheConfluenceUI
  [Confluence XML-RPC and SOAP APIs]: /confcloud/confluence-xml-rpc-and-soap-apis-39985844.html
  [JIRA JSON-RPC APIs]: https://developer.atlassian.com/display/JIRADEV/JIRA+JSON-RPC+Overview
  [Confluence JSON-RPC API Methods]: /confcloud/remote-confluence-methods-39985850.html
  [Confluence JSON-RPC API Data Objects]: /confcloud/remote-confluence-data-objects-39985848.html
  [Confluence SOAP API Javadoc]: https://developer.atlassian.com/display/HOME/API+Reference
  [documentation for the REST API Browser]: https://developer.atlassian.com/x/9oCI

