---
title: "Tip of the week: Configure your Java version in Tomcat"  
date: "2015-03-30T16:00:00+07:00"
author: "jgarcia"
categories: ["Java", "totw", "Server"]
---

During my tour of duty providing support for our Tomcat and Java based
behind-the-firewall apps, a common concern was the effect of updating Java or
the JVM in the host after installing the product, and how it may affect a
running production system. As some operating systems will auto-update system
libraries such as Java with little notice, this risks loss of service from
mission-critical apps in the unlikely case of a Java breaking change. Another
common scenario that comes up is a case where a specific Java version is
required for the app, which may not match the default version installed in the
host. In this case, we'll want to specify a Java path to override any
environment variable set at the host level to ensure consistent app behaviour.
Finally, admins who wish to deploy multiple apps that require different versions
of Java will be able to invoke them on the same host, each with the correct JVM.

An oft overlooked part of the [Apache Tomcat instructions][tomcat-instructions]
is the mention of the setenv file, which is typically pre-packaged with a
recommended configuration from the vendor (if you download [Stash][stash-url] or
our other products standalone, you'll see it's packaged in this way). Most folks
will just unpack the tarball, set a system environemnt variable to declare their
STASH_HOME, and be on their way. This works great for 90% of installs, but power
users will need to get into this file for a variety of reasons, such as
configuring Tomcat's memory utilization or GC implementation. This file can
typically be found at {app_install}/bin/setenv.sh or
{app_install}\bin\setenv.bat in Windows.

To configure the JAVA_HOME in *nix, open setenv.sh file in your favorite text
editor and add the following as a new first line:

    JAVA_HOME=/absolute/path/to/java  

Or in Windows, open setenv.bat and add the following at the top:

    set "JAVA_HOME=c:\path\to\Java" 

Once complete, save the change to the file and start (or restart) the server!

Let us know in the comments your use case for deterministically configuring your
Java!

Follow our [@atlassiandev](https://twitter.com/atlassiandev) Twitter handle for
updates about our fine line of developer tools.

Follow me on Twitter for updates about Atlassian software, robotics, Maker art,
and software philosophy at [@bitbucketeer](https://twitter.com/bitbucketeer).

[tomcat-instructions]: http://tomcat.apache.org/tomcat-7.0-doc/RUNNING.txt
[stash-url]: https://www.atlassian.com/software/stash
