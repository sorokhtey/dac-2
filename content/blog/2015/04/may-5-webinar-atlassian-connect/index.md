---
title: "Webinar: Building add-ons for Atlassian Cloud with Connect"
date: "2015-04-28T17:00:00+07:00"
author: "rwhitbeck"
categories: ["events", "Webinars", "atlassian-connect"]
---

Join us on May 5th at 10am PDT for a free webinar to learn more about developing for 
[Atlassian Connect](http://connect.atlassian.com). Atlassian Connect is Atlassian's next generation platform for 
building add-ons for Atlassian Cloud products.

We know you love JIRA and can't get enough Confluence, but did you ever have this 
feeling that you would work so much faster if JIRA did this, or Confluence showed that? 

With Atlassian Connect add-ons you can now add features, integrate with other systems, 
and scratch that itch. 

At the end of the webinar, you will understand the motivations and concepts behind 
Atlassian Connect, and know how to:

* Setup your local development environment for building add-ons
* Write a Connect add-on for JIRA and deploy it locally
* Deploy your add-on in a public cloud like Heroku or AWS
* Publish your add-on as a listing in the Atlassian Marketplace
* Use your add-on in a Cloud instance

<div style="text-align: center; margin-top: 20px;">
	<h3 style="margin-bottom: 20px;">Join us May 5th at 10am PDT</h3>

	<a href="https://www.atlassian.com/webinars/software/building-add-ons-for-atlassian-cloud-with-connect" class="aui-button aui-button-primary">Register Now</a>
</div>

<hr>

<br>