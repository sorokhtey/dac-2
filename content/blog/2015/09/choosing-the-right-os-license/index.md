---
title: "Choosing the right open source license"
date: "2015-09-11T16:00:00+07:00"
author: "vbadenhope"
categories: ["opensource"]
lede: "As Mandy mentioned in our last open source blog post, we've been working hard to get our open source 
house in order.  A core aspect of the tidying effort was settling on a uniform and consistent license 
framework for all our projects. Keep reading to see which license we selected and gain some insight into our 
decision making process."
---

As [Mandy](/blog/authors/mandyblack/) mentioned in our last open source blog post, we've been working hard to 
[get our open source house in order](/blog/2015/08/getting-os-house-in-order/).  A core aspect of the tidying 
effort was settling on a uniform and consistent license framework for all our projects.  Previously, licenses 
were applied to our projects on an ad hoc basis, and we were not always explicit about how we would accept 
contributions and under what terms.

![share](osdc_share_pixels.png) 
<p style="font-size: 0.8em;"><i>Image by:</i> opensource.com</p>

We realized that if we wanted to grow our open source program, we'd need to have a more consistent 
approach to licensing, not only to ease the burden of administering projects but to provide more clarity 
from a legal and IP perspective so that organizations can feel more comfortable participating. 

## We have chosen the Apache License for our projects
As an attorney who looks after Atlassian's own consumption of open source code, I appreciate it when 
projects anticipate the typical concerns that a commercial legal department might have:  what is the 
permitted scope of use, are there copyleft provisions to be concerned about, and how hard would it be 
to comply with the license terms.
 
To address these concerns, we wanted a common, well-understood, permissive license with the lowest 
barriers to adoption, including in commercial applications.  The obvious choices that meet these 
criteria are the MIT, BSD, and Apache licenses.  Any of these would have met our goals - all are 
non-copyleft licenses with minimal compliance requirements - but we chose the Apache license for its 
technical drafting; it contains formal license grant language that explicitly includes both copyright 
and patent and uses the corresponding statutory terminology accordingly.  In contrast, the MIT and BSD 
licenses rely on colloquial language and implied license grants, though their intent may be clear. 
 
Barring other considerations (and on an exception basis), we will apply the Apache License to software 
that Atlassian releases to the open source community.

## We accept contributions through an Apache-style contributor agreement
Another decision we had to make was the terms under which we would accept contributions.  Again, we wanted 
to follow well-established precedent that would not impede participation, and again we turned to the example 
set by the Apache Foundation (and followed by countless other active and successful projects).  Our "individual" 
and "corporate" contribution agreements mirror the Apache contribution agreements almost exactly and allow 
contributors to maintain ownership in the IP of their contributions while granting Atlassian the rights to 
control and maintain our projects. 
 
We considered simply accepting contributions under the same license that covers our projects (generally the 
Apache License, as discussed above), but opted for the additional assurances included in the contribution 
agreements (such as the representations that the contribution is an original work of the contributor and that 
the contributor has the legal right to make the contribution).  These provisions are significant to maintaining 
the overall IP "hygiene" of our projects.
 
## We want our projects to take center stage, not our licensing choices
We've deliberately chosen what we think is a plain vanilla legal framework for our open source program.  We're 
not looking to break new legal ground or court any controversy.  We just want you to engage and participate in 
our open source community, and we hope we have lowered any legal barriers you may face in doing so.

