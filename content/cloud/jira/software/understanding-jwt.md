---
title: Understanding JWT
platform: cloud
product: jswcloud
category: devguide
subcategory: security
date: "2016-11-02"
---
{{< include path="content/cloud/connect/concepts/understanding-jwt.snippet.md" >}}