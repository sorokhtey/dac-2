---
title: OAuth for REST APIs
platform: cloud
product: jswcloud
category: devguide
subcategory: security
date: "2016-11-02"
---
{{< reuse-page path="content/cloud/jira/platform/jira-rest-api-oauth-authentication.md">}}