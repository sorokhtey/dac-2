---
title: Architecture overview
platform: cloud
product: jsdcloud
category: devguide
subcategory: intro
date: "2016-10-28"
---
{{< include path="content/cloud/connect/concepts/cloud-development.snippet.md">}}
