---
title: Scopes
platform: cloud
product: jsdcloud
category: devguide
subcategory: blocks
aliases:
- /cloud/jira/platform/scopes.html
- /cloud/jira/platform/scopes.md
date: "2016-10-31"
---
{{< include path="content/cloud/connect/reference/scopes.snippet.md">}}