# Micros sends HTTP requests to 8081 if enabled. See allowInsecureHttpConnection in service-descriptor.yaml and
# https://extranet.atlassian.com/questions/2698709017/how-do-i-make-micros-redirect-http-requests-to-https
<VirtualHost *:8081>
    Redirect permanent / https://${APACHE_SERVER_NAME}/
</VirtualHost>


<VirtualHost *:8080>

    DocumentRoot /var/www/html

    # See micros-logging conf fragment
    CustomLog /dev/stdout json_format
    <IfModule log_forensic_module>
      ForensicLog /dev/stderr
    </IfModule>

    # Enable HSTS; see https://extranet.atlassian.com/questions/2698709017/how-do-i-make-micros-redirect-http-requests-to-https
    Header always set Strict-Transport-Security "max-age=63072000; includeSubdomains;"

    # Redirect directories to the slashed version. DirectorySlash
    # doesn't play well with port redirecting, so use rewrite for more
    # control.
    DirectorySlash off
    RewriteEngine on
    RewriteCond %{DOCUMENT_ROOT}%{REQUEST_FILENAME} -d
    RewriteCond %{REQUEST_URI} !(.*)/$
    RewriteRule ^(.*)$ https://%{HTTP:host}%{REQUEST_URI}/ [R=301,L]

</VirtualHost>
